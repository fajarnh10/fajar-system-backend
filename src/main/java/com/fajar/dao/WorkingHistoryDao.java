package com.fajar.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.fajar.model.WorkingHistory;
import com.fajar.model.json.workingHistory.sub.WorkingHistoryDataModel;

@Mapper
public interface WorkingHistoryDao {

	public WorkingHistory getByWorkId(
			@Param("workId")String workId);
	
	public List<WorkingHistoryDataModel> getAll();
	
}
