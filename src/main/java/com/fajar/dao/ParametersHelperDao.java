package com.fajar.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface ParametersHelperDao {

	 public String getValue(
			 @Param("need") String need,
			 @Param("name") String name);
		
}
