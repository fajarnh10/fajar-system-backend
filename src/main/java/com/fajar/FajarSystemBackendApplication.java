package com.fajar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FajarSystemBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(FajarSystemBackendApplication.class, args);
	}

}
