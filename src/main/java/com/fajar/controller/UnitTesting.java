package com.fajar.controller;

import java.io.IOException;
import java.util.Date;
import java.util.Scanner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fajar.model.json.unitTesting.UTStatusModel;
import com.fajar.model.json.unitTesting.UTStudentRequestModel;
import com.fajar.model.json.unitTesting.UTStudentResponseModel;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

@Controller
public class UnitTesting {

	private static final Logger logger = LoggerFactory.getLogger(UnitTesting.class);
	
	@RequestMapping(value="/api/example/v1/student", method = RequestMethod.POST)
	public void exampleApiOne(HttpServletRequest request, HttpServletResponse response) {
		logger.info("===================== START API EXAMPLE V1 STUDENT =====================");
		
		ObjectMapper mapper = new ObjectMapper();
		UTStudentResponseModel responseModel = new UTStudentResponseModel();
		UTStudentRequestModel requestModel = new UTStudentRequestModel();
		UTStatusModel statusModel = new UTStatusModel();
		statusModel.setCode("FAILED");
		statusModel.setDescription("Undefined Error");
		
		try {
			StringBuffer sb = new StringBuffer();
			@SuppressWarnings("resource")
			Scanner scanner = new Scanner(request.getInputStream());
		    
			while (scanner.hasNextLine())
		        sb.append(scanner.nextLine());
		    String body = sb.toString();
		    logger.info("request body {}", body);
		    
		    if (body==null||"".equals(body.replace(" ", ""))) {
				logger.error("Request body is empty or null");
				statusModel.setDescription("Request Body Cannot Empty or Null");
				responseModel.setStatus(statusModel);
			} else {
				requestModel = mapper.readValue(body, UTStudentRequestModel.class);
				if (requestModel.getNim() != null && !requestModel.getNim().isEmpty()) {
					statusModel.setCode("SUCCESS");
					statusModel.setDescription("Request is success");
					responseModel.setStatus(statusModel);
					responseModel.setNim("068016026");
					responseModel.setName("Fajar Nur Hidayat");
					responseModel.setMinor("Information System");
					responseModel.setMajor("Technical Information Computer");
					responseModel.setSemester("7");
				} else {
					logger.error("Value request model body is empty or null");
					statusModel.setDescription("Value Request Body Cannot Empty or Null");
					responseModel.setStatus(statusModel);
				}
			}
		    
		    mapper.enable(SerializationFeature.INDENT_OUTPUT);
		    String resp = mapper.writeValueAsString(responseModel);
		    logger.info("return response "+resp);
			responseJSON(response, resp);
		} catch (Exception e) {
			logger.error("Error when get student : \n",e);
		}
		
		logger.info("===================== END API EXAMPLE V1 STUDENT =====================");
	}
	
	@RequestMapping(value="/api/example/v2/student", method = RequestMethod.POST)
	public @ResponseBody UTStudentResponseModel exampleApiTwo(HttpServletRequest request, HttpServletResponse response) {
		logger.info("===================== START API EXAMPLE V2 STUDENT =====================");
		
		ObjectMapper mapper = new ObjectMapper();
		UTStudentResponseModel responseModel = new UTStudentResponseModel();
		UTStudentRequestModel requestModel = new UTStudentRequestModel();
		UTStatusModel statusModel = new UTStatusModel();
		statusModel.setCode("FAILED");
		statusModel.setDescription("Undefined Error");
		
		try {
			StringBuffer sb = new StringBuffer();
			@SuppressWarnings("resource")
			Scanner scanner = new Scanner(request.getInputStream());
		    
			while (scanner.hasNextLine())
		        sb.append(scanner.nextLine());
		    String body = sb.toString();
		    logger.info("request body : " + body);
		    
		    if (body==null||"".equals(body.replace(" ", ""))) {
				logger.error("Request body is empty or null");
				statusModel.setDescription("Request Body Cannot Empty or Null");
				responseModel.setStatus(statusModel);
			} else {
				requestModel = mapper.readValue(body, UTStudentRequestModel.class);
				if (requestModel.getNim() != null && !requestModel.getNim().isEmpty()) {
					statusModel.setCode("SUCCESS");
					statusModel.setDescription("Request is success");
					responseModel.setStatus(statusModel);
					responseModel.setNim("068016026");
					responseModel.setName("Fajar Nur Hidayat");
					responseModel.setMinor("Information System");
					responseModel.setMajor("Technical Information Computer");
					responseModel.setSemester("7");
				} else {
					logger.error("Value request model body is empty or null");
					statusModel.setDescription("Value Request Body Cannot Empty or Null");
					responseModel.setStatus(statusModel);
				}
			}
		    logger.info("Response : " + mapper.writeValueAsString(responseModel));
		} catch (Exception e) {
			logger.error("Error when get student : \n",e);
		}
		logger.info("===================== END API EXAMPLE V2 STUDENT =====================");
		return responseModel;
	}
	
	@RequestMapping(value = "/api/example/v3/student", method = RequestMethod.POST)
	public @ResponseBody UTStudentResponseModel exampleApiThree (
			@RequestBody UTStudentRequestModel request,
			HttpServletRequest requestContext) {
		
		logger.info("===================== START API EXAMPLE V3 STUDENT =====================");
		ObjectMapper mapper = new ObjectMapper();
		UTStudentResponseModel responseModel = new UTStudentResponseModel();
		UTStatusModel statusModel = new UTStatusModel();
		statusModel.setCode("FAILED");
		statusModel.setDescription("Undefined Error");
		
		try {
			logger.info("Incomming request : " + mapper.writeValueAsString(request));
			if (request.getNim() != null && !request.getNim().isEmpty()) {
				statusModel.setCode("SUCCESS");
				statusModel.setDescription("Request is success");
				responseModel.setStatus(statusModel);
				responseModel.setNim("068016026");
				responseModel.setName("Fajar Nur Hidayat");
				responseModel.setMinor("Information System");
				responseModel.setMajor("Technical Information Computer");
				responseModel.setSemester("7");
			} else {
				logger.error("Value request model body is empty or null");
				statusModel.setDescription("Value Request Body Cannot Empty or Null");
				responseModel.setStatus(statusModel);
			}
		    logger.info("Response : " + mapper.writeValueAsString(responseModel));
		} catch (Exception e) {
			logger.error("Error when get student : \n",e);
		}
		logger.info("===================== END API EXAMPLE V3 STUDENT =====================");
		return responseModel;
	}
	
	private void responseJSON(HttpServletResponse response, String json) {
		response.setStatus(HttpServletResponse.SC_OK); // status
		response.setDateHeader("Date", new Date().getTime()); // Date
		response.setContentType("application/json"); // content
		response.setContentLength(json.length()); // content-length
		try {
			response.getWriter().print(json);
			response.getWriter().flush();
			response.getWriter().close();
		} catch (IOException e) {
			logger.error("Error while generate json", e);
		}
	}
	
}
