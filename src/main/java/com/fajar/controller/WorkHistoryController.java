package com.fajar.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fajar.constant.ConstantCodes;
import com.fajar.constant.ConstantHelper;
import com.fajar.constant.ConstantSymbol;
import com.fajar.dao.ParametersHelperDao;
import com.fajar.dao.WorkingHistoryDao;
import com.fajar.model.WorkingHistory;
import com.fajar.model.json.workingHistory.GeneralWorkingHistoryModelRequest;
import com.fajar.model.json.workingHistory.GeneralWorkingHistoryModelResponse;
import com.fajar.model.json.workingHistory.sub.WorkingHistoryDataModel;
import com.fajar.util.Aes128Bit;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/v1/workingHistory")
public class WorkHistoryController {
	
	protected static final Logger logger = LoggerFactory.getLogger(WorkHistoryController.class);
	
	@Autowired
	private ParametersHelperDao paramHelper;
	
	@Autowired
	private WorkingHistoryDao workingHistoryDao;
	
	@Autowired
	private ObjectMapper mapper = new ObjectMapper();
	
	@RequestMapping(value = "/hello/world", method=RequestMethod.POST)
	public String helloWorld() {
		String returnValue = paramHelper.getValue(
				ConstantHelper.CONST_NEED_CONNECTOR, 
				ConstantHelper.CONST_NAME_FAJAR + 
				ConstantSymbol.CONST_SYMBOL_DOT + 
				ConstantHelper.CONST_CONNECTOR_FAJAR_CLIENT_ID);
		
		return returnValue;
	}
	
	@RequestMapping(value = "/inquiry", method=RequestMethod.POST)
	public  @ResponseBody GeneralWorkingHistoryModelResponse getWorkingHistoryById(
			@RequestBody GeneralWorkingHistoryModelRequest request,
			@RequestHeader(ConstantHelper.CONST_TEXT_TIMESTAMP) String timestamp,
			@RequestHeader(ConstantHelper.CONST_TEXT_CLIENT_ID) String clientId,
			@RequestHeader(ConstantHelper.CONST_TEXT_SIGNATURE) String signature,
			HttpServletRequest requestContext) {
		
		logger.info("========== START INQRUI WORKING HISTORY START ==========");
		WorkingHistoryDataModel responseData = new WorkingHistoryDataModel();
		List<WorkingHistoryDataModel> responseDataList = new ArrayList<WorkingHistoryDataModel>();
		responseDataList.add(responseData);
		GeneralWorkingHistoryModelResponse response = new GeneralWorkingHistoryModelResponse();
		response.setStatus(ConstantCodes.CONST_STRING_CODE_GENERAL_ERROR);
		response.setData(responseDataList);
		
		try {
			if (timestamp == null || timestamp.isEmpty()) {
				logger.error("TIMESTAMP NULL");
				response.setStatus(ConstantCodes.CONST_STRING_CODE_TIMESTAMP_NULL);
				
				return response;
			}
			
			if (clientId == null || clientId.isEmpty()) {
				logger.error("CLIENT ID NULL");
				response.setStatus(ConstantCodes.CONST_STRING_CODE_CLIENT_ID_NULL);
				
				return response;
			}
			
			if (signature == null || signature.isEmpty()) {
				logger.error("SIGNATURE NULL");
				response.setStatus(ConstantCodes.CONST_STRING_CODE_SIGNATURE_NULL);
				
				return response;
			}
			
			if (request.getWorkingId() == null || request.getWorkingId().isEmpty()) {
				logger.error("WORKING ID NULL");
				response.setStatus(ConstantCodes.CONST_STRING_CODE_WORKING_ID_NULL);
				
				return response;
			}
			
			try {
				String plainText = clientId + timestamp;
				String generateSignature = Aes128Bit.encrypt(plainText, clientId);
				logger.info("GENERATE SIGNATURE : {}", generateSignature);
				logger.info("REQUEST SIGNATURE : {}", signature);
				if (!signature.equals(generateSignature)) {
					logger.error("SIGNATURE IS NOT MATCH");
					response.setStatus(ConstantCodes.CONST_STRING_CODE_SIGNATURE_NOT_MATCH);
					return response;
				}
			} catch (Exception e) {
				logger.error("ERROR WHEN VALIDATION SIGNATURE : \n",e);
				return response;
			}
			
			try {
				WorkingHistory workingData =  workingHistoryDao.getByWorkId(request.getWorkingId());
				logger.info("WORKING DATA : " + mapper.writeValueAsString(workingData));
				if (workingData == null) {
					logger.error("DATA IS NOT FOUND");
					response.setStatus(ConstantCodes.CONST_STRING_CODE_DATA_NOT_FOUND);
					return response;
				}
				
				responseData.setWorkId(String.valueOf(workingData.getWorkId()));
				responseData.setCompanyName(workingData.getCompanyName());
				responseData.setPosition(workingData.getPosition());
				
				Date joinDate = workingData.getJoinDate();
				DateFormat dateFormat = new SimpleDateFormat(ConstantSymbol.CONST_SYMBOL_ISO_8601_YYYYMM);  
		        String strJoinDate = dateFormat.format(joinDate);
				responseData.setJoinDate(strJoinDate);
				
				responseData.setDescription(workingData.getDescription());
				String strResignDate = ConstantSymbol.CONST_SYMBOL_EMPTY;
				if (workingData.getResignDate() != null) {
					Date resignDate = workingData.getResignDate();
					strResignDate = dateFormat.format(resignDate);
				}
				responseData.setResignDate(strResignDate);
				response.setStatus(ConstantCodes.CONST_STRING_CODE_SUCCESS);
				
				logger.info("========== END INQRUI WORKING HISTORY END ==========");
				return response;
			} catch (Exception e) {
				logger.error("ERROR WHEN GET DATA : \n",e);
				return response;
			}
		} catch (Exception e) {
			logger.error("ERROR WHEN INQUIRY WORKING HISTORY : \n",e);
			return response;
		}
	}
	
	@RequestMapping(value = "/getAll", method=RequestMethod.POST)
	public GeneralWorkingHistoryModelResponse getAllWorkingHistory(
			@RequestHeader(ConstantHelper.CONST_TEXT_TIMESTAMP) String timestamp,
			@RequestHeader(ConstantHelper.CONST_TEXT_CLIENT_ID) String clientId,
			@RequestHeader(ConstantHelper.CONST_TEXT_SIGNATURE) String signature) {
		
		logger.info("========== START GET ALL WORKING HISTORY START ==========");
		GeneralWorkingHistoryModelResponse response = new GeneralWorkingHistoryModelResponse();
		
		try {
			if (timestamp == null || timestamp.isEmpty()) {
				logger.error("TIMESTAMP NULL");
				response.setStatus(ConstantCodes.CONST_STRING_CODE_TIMESTAMP_NULL);
				
				return response;
			}
			
			if (clientId == null || clientId.isEmpty()) {
				logger.error("CLIENT ID NULL");
				response.setStatus(ConstantCodes.CONST_STRING_CODE_CLIENT_ID_NULL);
				
				return response;
			}
			
			if (signature == null || signature.isEmpty()) {
				logger.error("SIGNATURE NULL");
				response.setStatus(ConstantCodes.CONST_STRING_CODE_SIGNATURE_NULL);
				
				return response;
			}
			
			try {
				String plainText = clientId + timestamp;
				String generateSignature = Aes128Bit.encrypt(plainText, clientId);
				logger.info("GENERATE SIGNATURE : {}", generateSignature);
				logger.info("REQUEST SIGNATURE : {}", signature);
				if (!signature.equals(generateSignature)) {
					logger.error("SIGNATURE IS NOT MATCH");
					response.setStatus(ConstantCodes.CONST_STRING_CODE_SIGNATURE_NOT_MATCH);
					return response;
				}
			} catch (Exception e) {
				logger.error("ERROR WHEN VALIDATION SIGNATURE : \n",e);
				return response;
			}
			
			try {
				List<WorkingHistoryDataModel> dataList = workingHistoryDao.getAll();
				if (dataList == null || dataList.size() == 0) {
					logger.info("DATA WORKING HISTORY IS NULL");
					response.setStatus(ConstantCodes.CONST_STRING_CODE_DATA_NOT_FOUND);
					return response;
				}
				response.setData(dataList);
			} catch (Exception e) {
				logger.error("ERROR WHEN GET DATA FORM DB");
				return response;
			}
			
			logger.info("========== END GET ALL WORKING HISTORY END ==========");
			return response;
		} catch (Exception e) {
			logger.error("ERROR WHEN GET ALL DATA : \n",e);
			return response;
		}
	}
}
